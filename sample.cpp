#include "threading.h"
using namespace std;
// Test classes derived from Threding
class TestThreading1 : public Threading
{
	void threadFunc(void)
	{
		for (int i = 0; i < 10; i++)
		{
			cout << "Test1: " << i << endl;
			// wait 1 sec
			boost::this_thread::sleep(boost::posix_time::millisec(1000));
			createBreakPoint();
		}
	}
};

class TestThreading2 : public Threading
{
	void threadFunc(void)
	{
		for (int i = 0; i < 10; i++)
		{
			cout << "Test2: " << i << endl;
			// wait 2 sec
			boost::this_thread::sleep(boost::posix_time::millisec(2000));
			createBreakPoint();
		}
	}
};

int main(void)
{
	TestThreading1 test1;
	TestThreading2 test2;

	test1.BeginThread();
	boost::this_thread::sleep(boost::posix_time::millisec(500));
	test2.BeginThread();

	// wait 5 sec
	boost::this_thread::sleep(boost::posix_time::millisec(5000));
	// suspend test1 5 sec
	cout << "wait 5 sec" << endl;
	test1.SuspendThread();
	boost::this_thread::sleep(boost::posix_time::millisec(5000));
	// resume test1
	cout << "resume" << endl;
	test1.ResumeThread();

	// wait for the terminations
	test1.WaitTermination();
	test2.WaitTermination();
}