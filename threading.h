#pragma once
#include <boost/thread.hpp>

enum ThreadStatus
{
	STOP,
	SUSPEND,
	RUNNING
};

class Threading
{
public:
	Threading(void);
	virtual ~Threading(void);

	void BeginThread(void);
	void EndThread(void);
	void SuspendThread(void);
	void ResumeThread(void);
	void WaitTermination(void);
	ThreadStatus IsRunning(void){ return m_status; }

protected:
	virtual void threadFunc(void);
	virtual void initialize(void){}


	// for suspending and resuming thread
	virtual void createBreakPoint(void);
	ThreadStatus m_status;
	boost::condition_variable m_condResuming;
	boost::mutex m_mutexResuming;
	
	boost::thread m_thread;
};

