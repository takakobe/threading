#include "threading.h"

using namespace boost;
using namespace std;

Threading::Threading(void)
{
	m_status = STOP;
}


Threading::~Threading(void)
{
	if (m_status == SUSPEND) this->ResumeThread();
	if (m_status == RUNNING) this->EndThread();
}

void Threading::BeginThread(void)
{
	if (m_thread.joinable())
	{
#ifdef _DEBUG
		cerr << "A thread has already been running." << endl;
#endif
		return;
	}
	initialize();
	m_thread = boost::thread(boost::bind(&Threading::threadFunc, this));
	m_status = RUNNING;
}

void Threading::EndThread(void)
{
	if (!m_thread.joinable())
	{
#ifdef _DEBUG
		cerr << "No thread is running." << endl;
#endif
		return;
	}
	m_thread.interrupt();
	m_thread.join();
	m_status = STOP;
}

void Threading::SuspendThread(void)
{
	if (!m_thread.joinable() || m_status == SUSPEND)
	{
#ifdef _DEBUG
		cerr << "No thread is running or this thread is already suspended." << endl;
#endif
			return;
	}
	m_status = SUSPEND;
}

void Threading::ResumeThread(void)
{
	if (!m_thread.joinable() || m_status != SUSPEND)
	{
#ifdef _DEBUG
		cerr << "No thread is running or this thread is not suspended." << endl;
#endif
			return;
	}
	m_status = RUNNING;
	m_condResuming.notify_one();
}

void Threading::WaitTermination(void)
{
	m_thread.join();
}

void Threading::threadFunc(void)
{
	int i = 0;
	while (1)
	{
		i++;
		createBreakPoint();
		cout << "A" << i << endl;
		this_thread::sleep(posix_time::milliseconds(1000));
	}
}

void Threading::createBreakPoint(void)
{
	// for ending thread
	this_thread::interruption_point();

	// for suspending and resuming thread
	if (m_status == SUSPEND)
	{
		unique_lock<mutex> lock(m_mutexResuming);
		m_condResuming.wait(lock);
	}
}